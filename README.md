


#Modelo de projeto Laravel


#Adicionado Laravel 5.7
composer create-project --prefer-dist laravel/laravel

#Criar arquivo .env
configurar banco de dados usuário e senha

#Configuração da chave de criptografia
php artisan key:generate

#Adicionado ACL
composer require westsoft/acl

#Publicar vendor
php artisan vendor:publish

#Auth
php artisan make:auth

#Migration
php artisan migrate

#Seeder
php artisan db:seed



#Adicionar route middleware em Kernel.php
'check.permissions' => Westsoft\Acl\Middleware\CheckPermissions::class,


