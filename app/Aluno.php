<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aluno extends Model
{
    protected $table = "alunos";

    protected $fillable = [
        'nome',
        'rg',
        'cpf',
        'sexo',
        'cep',
        'endereco',
        'cidade',
        'telefone',
        'celular',
        'email',
        'tamanho_camiseta',//fazer no formulário
        'possui_necessidade_especial',
        'necessidade_especial',
        'data_nascimento',
    ];


    public function projetos()
    {
        return $this->belongsToMany('App\Projeto','project_alunos', 'aluno_id', 'project_id')->withTimestamps();
    }

}
