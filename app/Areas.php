<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    protected $table = "areas";

    protected $fillable = [
        'nome',
    ];    

    public function subarea()
    {
        return $this->hasMany('App\Subareas', 'area_id', 'id');
    }
}
