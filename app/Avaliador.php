<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avaliador extends Model
{

        protected $table = "avaliadores";
    
        protected $fillable = [
            'nome',
            'rg',
            'cpf',
            'sexo',
            'cep',
            'endereco',
            'cidade',
            'telefone',
            'celular',
            'email',
            'tamanho_camiseta',
            'possui_necessidade_especial',
            'necessidade_especial',
            'data_nascimento',
            'instituicao',
            'formacao',
            'ultima_graduacao',
            'area_id1',
            'area_id2',
        ];
    
    
     
    
    }
    