<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AlunoRequest;
use Westsoft\Acl\Models\UserProfiles;
use Illuminate\Support\Facades\Hash;
use App\Aluno;
use App\Projeto;
use Auth;
use App\User;

class AlunoController extends Controller
{
    public function index()
    {
        $alunos = Aluno::all();
        return view('aluno.index', compact('alunos'));
    }

    public function show($id)
    {
      /*  $projeto = projeto::find($id);
        
        //return response()->json($projeto);
        return view('projeto.show', compact('projeto'));*/
        return view('errors.404');

    }

    public function update(AlunoRequest $request, $id)
    {
        $data = $request->all();
        
        $aluno = Aluno::find($id);
                
        $aluno->update($data);

        return redirect('home')->with('success', 'Dados do aluno alterados.');
    }

    public function store(AlunoRequest $request)
    {
        $data = $request->all();
        
        $aluno = Aluno::create($data);

        if(Auth::id() == null){
            $user = User::create([
                'name' => $data['nome'],
                'email' => $data['email'],
                'password' => Hash::make($data['senha']),
            ]);
                    
            //$user_id = Auth::id();
            
            
            $data['user_id'] = $user->id;
    
    
            $user_profile['profiles_id'] = 4; //Aluno
            $user_profile['users_id'] = $user->id;
    
            $user_profiles = UserProfiles::create($user_profile);   
            
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['senha']]))
            {
                return redirect()->intended('home');
            }
        }

        if($projeto = Projeto::where('user_id', Auth::id())->first()){
            $projeto->alunos()->attach($aluno->id);
        }
        return redirect()->intended('home');
    }

    public function destroy($id)
    {      
        $aluno = Aluno::find($id);
                
        $aluno->delete();

        return redirect('home')->with('success', 'Aluno excluído.');
    }

    public function create()
    {
        return view('aluno.create-edit');
    }

    public function participante()
    {
        $participante = true;
        return view('aluno.create-edit', compact('participante'));
    }

    public function edit($id)
    {
        $aluno = Aluno::find($id);
        return view('aluno.create-edit', compact('aluno'));
    }
}
