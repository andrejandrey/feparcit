<?php

namespace App\Http\Controllers;

use App\User;
use App\Avaliador;
use App\Areas;
use Illuminate\Http\Request;
use App\Http\Requests\AvaliadorRequest;
use Westsoft\Acl\Models\UserProfiles;
use Illuminate\Support\Facades\Hash;
use Auth;

class AvaliadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avaliadores = Avaliador::all();
        return view('avaliador.index', compact('avaliadores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ultima_graduacao = array(
            [ "id" => "1", "name" => "Graduação em andamento"],
            [ "id" => "2", "name" => "Graduação"],
            [ "id" => "3", "name" => "Especialização (pós-graduação latu sensu)"],
            [ "id" => "4", "name" => "Mestrado"],
            [ "id" => "5", "name" => "Doutorado"],
        );
       
        $areas = Areas::all();
        return view('avaliador.create-edit', compact('areas', 'ultima_graduacao'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AvaliadorRequest $request)
    {
        $data = $request->all();
        
        $avaliador = Avaliador::create($data);
        
        $user = User::create([
            'name' => $data['nome'],
            'email' => $data['email'],
            'password' => Hash::make($data['senha']),
        ]);


        $data['user_id'] = $user->id;


        $user_profile['profiles_id'] = 2; //Avaliador
        $user_profile['users_id'] = $user->id;

        $user_profiles = UserProfiles::create($user_profile);

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['senha']]))
        {
            return redirect()->intended('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Avaliadores  $avaliadores
     * @return \Illuminate\Http\Response
     */
    public function show(Avaliadores $avaliadores)
    {
        return view('errors.404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Avaliadores  $avaliadores
     * @return \Illuminate\Http\Response
     */
    public function edit(Avaliadores $avaliadores)
    {
        $avaliador = Avaliador::find($id);
        return view('avaliador.create-edit', compact('avaliador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Avaliadores  $avaliadores
     * @return \Illuminate\Http\Response
     */
    public function update(AvaliadorRequest $request, Avaliadores $avaliadores)
    {
        $data = $request->all();
        
        $avaliador = Avaliador::find($id);
                
        $avaliador->update($data);

        return redirect('home')->with('success', 'Dados do avaliador alterados.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Avaliadores  $avaliadores
     * @return \Illuminate\Http\Response
     */
    public function destroy(Avaliadores $avaliadores)
    {
        $avaliador = Avaliador::find($id);
                
        $avaliador->delete();

        return redirect('home')->with('success', 'Avaliador excluído.');
    }
}
