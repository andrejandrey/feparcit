<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailUser;

class ContactController extends Controller
{
    public function post(Request $request)
    {
        //dd($request);
        $data = $request->all();
        Mail::to('feparcit@gmail.com')->send(new SendMailUser($data));
        return redirect()->back();
    }
}
