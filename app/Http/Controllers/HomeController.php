<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Aluno;
use App\Orientador;
use App\Projeto;
use Westsoft\Acl\Models\Permission;
use Westsoft\Acl\Models\ProfilePermissions;
use Westsoft\Acl\Models\UserProfiles;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('documentos');;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         *  Pega o Id do usuário logado no momento,
         *  só chegara neste método se estiver logado protegido
         *  pelo middleware auth no construtor.
         */
        $id = Auth::id();

        /**
         *  Busca todos os profiles que estão relacionados com o usuário logado
         */
        $profiles_user = UserProfiles::where('users_id', '=', $id)->get();
        
        $permissions = array();
        /**
         *  Para cada perfil do usuario busca as permissões
         */
        $profiles = array();
        foreach ($profiles_user as $pu) {
            /* Guarda os perfil do usuario para consoltar dados posteriores */
            $profiles[] = $pu->profiles_id;

            $profile_permissions = ProfilePermissions::where('profiles_id', $pu->profiles_id)->get();
            /**
             * Permissão do perfil busca o nome
             */
            foreach ($profile_permissions as $pp) {
                $aux = Permission::where('id', $pp->permissions_id)->first();
                /**
                 * Nome da permissão, ex: users.store
                 */
                $permissions[] = $aux->name;
            }
        }
        //Habilitarpara ver as permissões
        //dd($permissions);

        /**
         *  session()->put() - Cria o array com as permissões na sessão.
         */
        
        $request->session()->put('permissions', $permissions);
        /**
         *  Segurança: session->regenerate() - Ao fazer isso mesmo que a sessão seja
         *  roubada estará seguro pois a partir do momento que faz isso o identificador
         *  da sessão muda e automaticamente inválida a versão anterior.
         */
        $request->session()->regenerate();


        if (in_array(1, $profiles)) { //ADM
            return view('admin');
        }

        if (in_array(2, $profiles)) { //Avaliador

            return view('avaliador');
        }

        if (in_array(3, $profiles)) { //Voluntário
            return view('voluntario');
        }

        $orientador = array();
        $coorientador = array();

        if(!$projeto = Projeto::where('user_id', Auth::id())->first()){
            $alunos = null;
            $orientadores = null;
        }else{
            $alunos = $projeto->alunos;
            $orientadores = $projeto->orientadores;

            foreach ($orientadores as $key => $value) {
                if($value->tipo_orientador == 1){
                    $orientador[] = $value;
                }elseif($value->tipo_orientador == 2){
                    $coorientador[] = $value;
                }
            }
        }

        return view('home', compact('projeto','alunos','orientador','coorientador'));
    }

    public function documentos()
    {
        return view('documentos');
    }
}
