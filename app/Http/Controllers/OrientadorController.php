<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrientadorRequest;
use App\Orientador;
use App\Projeto;
use Auth;


class OrientadorController extends Controller
{
    public function index()
    {
        $orientadores = Orientador::all();
        
        return view('orientador.index', compact('orientadores'));
    }

    public function show($id)
    {
      /*  $projeto = projeto::find($id);
        
        //return response()->json($projeto);
        return view('projeto.show', compact('projeto'));*/
    }

    public function update(OrientadorRequest $request, $id)
    {
        $orientador = Orientador::find($id);

        $data = $request->all();
        $data['professor'] = 1;
        $orientador->update($data);

        return redirect('home')->with('success', 'Orientador adicionado com sucesso!');
    }

    public function store(OrientadorRequest $request)
    {
        $data = $request->all();
        $data['professor'] = 1;
        $orientador = Orientador::create($data);
                
        $user_id = Auth::id();
        $projeto = Projeto::where('user_id', $user_id)->first();


        $projeto->orientadores()->attach($orientador->id);

        return redirect('home')->with('success', 'Orientador adicionado com sucesso!');
    }

    public function destroy($id)
    {
       
    }

    public function create()
    {
        $projeto = Projeto::where('user_id', Auth::id())->first();
        
        $orientador = $projeto->orientadores;
        $tipo_orientador = 2;

        //dd($orientador);


        if($orientador != null)
        foreach ($orientador as $key => $value) {
            if($value->tipo_orientador == 1){
                $tipo_orientador = 1;
            }
        }

        return view('orientador.create-edit', compact('tipo_orientador'));
    }

    public function edit($id)
    {
       /*  $projeto = Projeto::where('user_id', Auth::id())->first(); */

        $orientador = Orientador::find($id);

/*         $orientadores = $projeto->orientadores;
        $tipo_orientador = 2;

        if($orientadores != null)
        foreach ($orientadores as $key => $value) {
            if($value->tipo_orientador == 1){
                $tipo_orientador = 1;
            }
        } */

        return view('orientador.create-edit', compact('orientador'));
    }
}
