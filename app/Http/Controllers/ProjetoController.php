<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjetoRequest;
use App\Projeto;
use App\Areas;
use App\Subareas;
use Auth;
use Illuminate\Support\Facades\Storage;

class ProjetoController extends Controller
{
    public function index()
    {
        $projetos = Projeto::all();
        
        return view('projeto.index', compact('projetos')); 
    }


    public function getSubAreas(Request $request)
    {
        $id = $request->area;
        

        $subareas = Subareas::where('area_id', '=', $id)->get();

        return response()->json($subareas);
    }

    public function show($id)
    {
        $projeto = projeto::find($id);
        
        //return response()->json($projeto);
        return view('projeto.show', compact('projeto'));
    }

    public function update(ProjetoRequest $request, $id)
    {
        $projeto = Projeto::find($id);

        $data = $request->all();

        if ($request->hasFile('cronograma')) {
            $path = $request->file('cronograma')->store('feparcit/cronograma');

            $url = Storage::url($path);

            $data['cronograma'] = $url;
        }

        if ($request->hasFile('parecer')) {
            $path = $request->file('parecer')->store('feparcit/parecer');

            $url = Storage::url($path);

            $data['parecer'] = $url;
        }

        if ($request->hasFile('anexo')) {
            $path = $request->file('anexo')->store('feparcit/anexo');

            $url = Storage::url($path);

            $data['anexo'] = $url;
        }
        $data['status']  = 'pendente';
        $data['user_id'] = Auth::id();
        
        $projeto->update($data);

        return redirect('home')->with('success', 'Projeto alterado com sucesso!');
    }

    public function store(ProjetoRequest $request)
    {
        $data = $request->all();

        if ($request->hasFile('cronograma')) {
            $path = $request->file('cronograma')->store('feparcit/cronograma');

            $url = Storage::url($path);

            $data['cronograma'] = $url;
        }

        if ($request->hasFile('parecer')) {
            $path = $request->file('parecer')->store('feparcit/parecer');

            $url = Storage::url($path);

            $data['parecer'] = $url;
        }

        if ($request->hasFile('anexo')) {
            $path = $request->file('anexo')->store('feparcit/anexo');

            $url = Storage::url($path);

            $data['anexo'] = $url;
        }
        $data['status']  = 'pendente';
        $data['user_id'] = Auth::id();
        $data['subarea_id'] = 1;
        
        $projeto = Projeto::create($data);

        return redirect('home')->with('success', 'Projeto adicionado');
    }

    public function destroy($id)
    {
       
    }

    public function create()
    {
        $tipo_pesquisa = array(
            [ "id" => "", "name" => "Escolha"],
            [ "id" => "1", "name" => "Pesquisa com seres humanos"],
            [ "id" => "2", "name" => "Pesquisa com animais vertebrados"],
            [ "id" => "3", "name" => "Pesquisa com agentes biologicos potencialmente perigosos"],
            [ "id" => "4", "name" => "Pesquisa com equipamentos ou substâncias controladas"],
            [ "id" => "5", "name" => "Pesquisa não envolve nenhum dos itens citados"],
        );
        $tipo_instituicao = array(
            [ "id" => "1", "name" => "Pública"],
            [ "id" => "2", "name" => "Privada"],
        );
        $areas = Areas::all();
        return view('projeto.create-edit', compact('tipo_pesquisa','tipo_instituicao','areas'));
    }

    public function edit($id)
    {
        $project = Projeto::find($id);
        $tipo_pesquisa = array(
            [ "id" => "", "name" => "Escolha"],
            [ "id" => "1", "name" => "Pesquisa com seres humanos"],
            [ "id" => "2", "name" => "Pesquisa com animais vertebrados"],
            [ "id" => "3", "name" => "Pesquisa com agentes biologicos potencialmente perigosos"],
            [ "id" => "4", "name" => "Pesquisa com equipamentos ou substâncias controladas"],
            [ "id" => "5", "name" => "Pesquisa não envolve nenhum dos itens citados"],
        );
        $tipo_instituicao = array(
            [ "id" => "1", "name" => "Pública"],
            [ "id" => "2", "name" => "Privada"],
        );
        $areas = Areas::all();
        return view('projeto.create-edit', compact('project', 'tipo_pesquisa','tipo_instituicao','areas'));
    }
}
