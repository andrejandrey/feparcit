<?php

namespace App\Http\Controllers;

use App\User;
use App\Voluntario;
use Illuminate\Http\Request;
use App\Http\Requests\VoluntarioRequest;
use Westsoft\Acl\Models\UserProfiles;
use Illuminate\Support\Facades\Hash;
use Auth;

class VoluntarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voluntarios = Voluntario::all();
        return view('voluntario.index', compact('voluntarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('voluntario.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VoluntarioRequest $request)
    {
        $data = $request->all();
        
        $voluntario = Voluntario::create($data);
             
        
        $user = User::create([
            'name' => $data['nome'],
            'email' => $data['email'],
            'password' => Hash::make('fwpmicsc'),
        ]);

        $data['user_id'] = $user->id;


        $user_profile['profiles_id'] = 3; //Voluntario
        $user_profile['users_id'] = $user->id;

        $user_profiles = UserProfiles::create($user_profile);

    
        if (Auth::attempt(['email' => $data['email'], 'password' => 'fwpmicsc']))
        {
            return redirect()->intended('home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voluntario  $voluntario
     * @return \Illuminate\Http\Response
     */
    public function show(Voluntario $voluntario)
    {
        return view('errors.404');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voluntario  $voluntario
     * @return \Illuminate\Http\Response
     */
    public function edit(Voluntario $voluntario)
    {
        $voluntario = Voluntario::find($id);
        return view('voluntario.create-edit', compact('voluntário'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voluntario  $voluntario
     * @return \Illuminate\Http\Response
     */
    public function update(VoluntarioRequest $request, Voluntario $voluntario)
    {
        $data = $request->all();
        
        $voluntario = Voluntario::find($id);
                
        $voluntario->update($data);

        return redirect('home')->with('success', 'Dados do voluntario alterados.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voluntario  $voluntario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voluntario $voluntario)
    {
        $voluntario = Voluntario::find($id);
                
        $voluntario->delete();

        return redirect('home')->with('success', 'Voluntario excluído.');
    }
}
