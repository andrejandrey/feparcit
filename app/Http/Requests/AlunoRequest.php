<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlunoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'nome'=> 'required|min:3|max:255',
                        'rg' => 'required|min:3|max:14',
                        'cpf'=> 'required|min:11|max:14',
                        'sexo'=> 'required',
                        'cep'=> 'required|min:8|max:10',
                        'endereco'=> 'required|min:3',
                        'cidade'=> 'required|min:3',
                        'telefone'=> 'required|min:3|max:20',
                        'celular'=> 'required|min:3|max:20',
                        'tamanho_camiseta'=> 'required',
                        'email'=> 'unique:users|required|min:3|max:255',
                        'senha'=> 'min:3|max:25',
                        'possui_necessidade_especial'=> 'required|integer',
                        'data_nascimento'=> 'required|date',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'nome'=> 'min:3|max:255',
                        'rg' => 'min:3|max:14',
                        'cep'=> 'min:8|max:10',
                        'endereco'=> 'min:3',
                        'cidade'=> 'min:3',
                        'telefone'=> 'min:3|max:20',
                        'celular'=> 'min:3|max:20',
                        'email'=> 'min:3|max:255',
                        'necessidade_especial'=> 'integer',
                        'data_nascimento'=> 'date',
                    ];
                }
            default:break;
        }
    }
}
