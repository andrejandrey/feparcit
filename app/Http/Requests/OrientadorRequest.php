<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrientadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'tipo_orientador' => 'required',
                        'nome'=> 'required|min:3|max:255',
                        'rg' => 'required|min:3|max:14',
                        'cpf'=> 'required|min:11|max:14',
                        'sexo'=> 'required',
                        'cep'=> 'required|min:8|max:10',
                        'endereco'=> 'required|min:3',
                        'cidade'=> 'required|min:3',
                        'telefone'=> 'required|min:3|max:20',
                        'celular'=> 'required|min:3|max:20',
                        'email'=> 'required|min:3|max:255',
                        'possui_necessidade_especial' => 'required',
                        'necessidade_especial'=> 'max:255',
                        'data_nascimento'=> 'required|date',
                        'tamanho_camiseta' => 'required',
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'tipo_orientador' => 'required',
                        'nome'=> 'min:3|max:255',
                        'rg' => 'min:3|max:14',
                        'cep'=> 'min:8|max:10',
                        'sexo'=> 'required',
                        'endereco'=> 'min:3',
                        'cidade'=> 'min:3',
                        'telefone'=> 'min:3|max:20',
                        'celular'=> 'min:3|max:20',
                        'email'=> 'min:3|max:255',
                        'possui_necessidade_especial'=> 'integer',
                        'necessidade_especial'=> 'max:255',
                        'data_nascimento'=> 'date',
                        'tamanho_camiseta' => 'required',
                    ];
                }
            default:break;
        }
    }
}
