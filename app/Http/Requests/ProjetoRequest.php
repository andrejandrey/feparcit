<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjetoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'categoria' => 'required|integer',
                        'area_id' => 'required|integer',
                        'subarea_id' => 'required|integer',
                        'nome'=> 'required|min:3|max:255',
                        'problema' => 'required|min:3',
                        'hipotese'=> 'required|min:3',
                        'materiais'=> 'required|min:3',
                        'metodos'=> 'required|min:3',
                        'bibliografia'=> 'required|min:3',
                        'resumo'=> 'required|min:3|max:2000',
                        'cronograma'=> 'required|file',
                        'resultado'=> 'required|min:3',
                        'palavras_chave'=> 'required|min:3',
                        'anexo'=> 'required|file',
                        'tipo_pesquisa' => 'required|integer',
                        'tipo_instituicao'=> 'required|integer',
                        'nome_instituicao'=>'required|min:3|max:255',
                        'email_instituicao'=>'email|required|min:3|max:255',
                        'telefone_instituicao'=>'required|min:3|max:255',
                       
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'nome'=> 'min:3|max:255',
                        'problema' => 'min:3',
                        'hipotese'=> 'min:3',
                        'materiais'=> 'min:3',
                        'metodos'=> 'min:3',
                        'bibliografia'=> 'min:3',
                        'resumo'=> 'min:3',
                        'cronograma'=> 'min:3',
                        'resultado'=> 'min:3',
                        'palavras_chave'=> 'min:3',
                        'anexo'=> 'file',
                        'tipo_pesquisa' => 'integer',
                        'tipo_instituicao'=> 'integer',
                        'nome_instituicao'=>'min:3|max:255',
                        'email_instituicao'=>'email|min:3|max:255',
                        'telefone_instituicao'=>'min:3|max:255',
                    ];
                }
            default:break;
        }
    }
}
