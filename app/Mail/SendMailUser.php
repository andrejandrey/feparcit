<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailUser extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
 
    /**
     * Create a new message instance.
     * @param User $user
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //dd($this->data);
        return $this->from($this->data['email'])->to('feparcit.contato@gmail.com')->subject($this->data['subject'])->view('emails.contact')->with([
            'data' => $this->data,
        ]);
    }
}
