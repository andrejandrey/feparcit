<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orientador extends Model
{
    protected $table = "orientadors";

    protected $fillable = [
        'tipo_orientador',
        'nome',
        'rg',
        'cpf',
        'sexo',
        'cep',
        'endereco',
        'cidade',
        'telefone',
        'celular',
        'email',
        'tamanho_camiseta', //Fazer no formulário
        'possui_necessidade_especial',
        'necessidade_especial',
        'data_nascimento',
        'professor',
    ];


    public function projetos()
    {
        return $this->belongsToMany('App\Projeto','project_orientadors', 'orientador_id', 'project_id')->withTimestamps();
    }
}
