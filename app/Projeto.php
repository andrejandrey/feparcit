<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    protected $table = "projects";

    protected $fillable = [
        'nome',
        'problema',
        'hipotese',
        'materiais',
        'metodos',
        'bibliografia',
        'resumo',
        'cronograma', //PDF //
        'resultado',// 
        'palavras_chave',
        'anexo',//1 arquivo
        'tipo_pesquisa',// 
        'parecer',// Arquivo com paracer da comissão de ética
        'tipo_instituicao',//público e privado
        'nome_instituicao',
        'email_instituicao',
        'telefone_instituicao',
        'status',
        'user_id',
        'subarea_id',
        'categoria', // Ensino médio, Subsequente
        //resumo campo aberto.
        //relatorio final anexo.

        //comentario dos avaliadores....
    ];    

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function subarea()
    {
        return $this->belongsTo('App\Subareas', 'subarea_id');
    }

    public function alunos()
    {
        return $this->belongsToMany('App\Aluno','project_alunos', 'project_id', 'aluno_id')->withTimestamps();
    }

    public function orientadores()
    {
        return $this->belongsToMany('App\Orientador','project_orientadors', 'project_id', 'orientador_id')->withTimestamps();
    }
}