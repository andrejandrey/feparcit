<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subareas extends Model
{
    protected $table = "subareas";

    protected $fillable = [
        'nome', 'area_id',
    ];    

    public function area()
    {
        return $this->belongsTo('App\Areas', 'area_id');
    }
}
