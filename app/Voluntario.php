<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voluntario extends Model
{
    protected $table = "voluntarios";

    protected $fillable = [
        'nome',
        'rg',
        'cpf',
        'sexo',
        'cep',
        'endereco',
        'cidade',
        'telefone',
        'celular',
        'email',
        'tamanho_camiseta',//fazer no formulário
        'possui_necessidade_especial',
        'necessidade_especial',
        'data_nascimento',
    ];
}
