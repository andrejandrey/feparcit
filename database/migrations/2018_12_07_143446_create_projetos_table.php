<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {   
            //Dados do projeto   
            $table->increments('id');
            $table->integer('categoria'); //1-Integrado 2-Subsequente
            $table->string('nome');
            $table->longText('problema');
            $table->longText('hipotese');
            $table->longText('metodos');
            $table->longText('materiais');
            $table->longText('bibliografia');
            $table->longText('resumo');
            $table->string('cronograma');
            $table->longText('resultado');
            $table->longText('palavras_chave');
            $table->string('anexo');

            //dados da instituicao
            $table->integer('tipo_pesquisa'); // Ver possibilidades?
            $table->string('parecer')->default(''); //Caso pesquisa com animais seres humanos ou perigosas anexar parecer file

            $table->integer('tipo_instituicao'); // Escola pública escola privada?
            $table->string('nome_instituicao');
            $table->string('email_instituicao');
            $table->string('telefone_instituicao');
            
            $table->string('status')->default('pendente'); //pendente, aprovado, negado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
