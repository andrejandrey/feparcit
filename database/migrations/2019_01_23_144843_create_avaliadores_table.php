<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliadores', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nome');
                $table->string('rg');
                $table->string('cpf')->nullable();
                $table->string('sexo');
                $table->string('cep');
                $table->string('endereco');
                $table->string('cidade');
                $table->string('telefone');
                $table->string('celular');
                $table->string('email');
                $table->string('possui_necessidade_especial');
                $table->string('necessidade_especial')->nullable();
                $table->string('tamanho_camiseta');
                $table->date('data_nascimento');

                $table->string('instituicao')->nullable();
                $table->string('formacao')->nullable();
                $table->string('ultima_graduacao')->nullable(); // Graduação em andamento, Graduação, especialização (pós-graduação latu sensu), mestrado e doutorado 

                $table->integer('area_id1')->unsigned();
                $table->foreign('area_id1')->references('id')->on('areas')->onDelete('cascade');

                $table->integer('area_id2')->unsigned();
                $table->foreign('area_id2')->references('id')->on('areas')->onDelete('cascade');

                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliadores');
    }
}
