<?php

use Illuminate\Database\Seeder;

class Areas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areas')->insert([
            ['id' => '1', 'nome' => 'Ciências Agrárias'],
            ['id' => '2', 'nome' => 'Ciências Biológicas'],
            ['id' => '3', 'nome' => 'Ciências da Saúde'],
            ['id' => '4', 'nome' => 'Ciências Exatas e da Terra'],
            ['id' => '5', 'nome' => 'Ciências Humanas'],
            ['id' => '6', 'nome' => 'Ciências Sociais'],
            ['id' => '7', 'nome' => 'Engenharias e suas Aplicações'],
        ]);
    }
}
