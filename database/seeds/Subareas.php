<?php

use Illuminate\Database\Seeder;

class Subareas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    


        DB::table('subareas')->insert([

            // 1 - Ciências Agrárias
            ['id' => '1', 'nome' => 'Agrometeorologia', 'area_id' => '1'],
            ['id' => '2', 'nome' => 'Agronomia', 'area_id' => '1'],
            ['id' => '3', 'nome' => 'Aquicultura', 'area_id' => '1'],
            ['id' => '4', 'nome' => 'Ciência do Solos', 'area_id' => '1'],
            ['id' => '5', 'nome' => 'Ciência e Tecnologia de Alimentos', 'area_id' => '1'],
            ['id' => '6', 'nome' => 'Conservação da Natureza', 'area_id' => '1'],
            ['id' => '7', 'nome' => 'Construções Rurais e Ambiência', 'area_id' => '1'],
            ['id' => '8', 'nome' => 'Ecologia dos Animais Domésticos e Etologia', 'area_id' => '1'],
            ['id' => '9', 'nome' => 'Energização Rural', 'area_id' => '1'],
            ['id' => '10', 'nome' => 'Extensão Rural', 'area_id' => '1'],
            ['id' => '11', 'nome' => 'Fitossanidade', 'area_id' => '1'],
            ['id' => '12', 'nome' => 'Fitotecnia', 'area_id' => '1'],
            ['id' => '13', 'nome' => 'Parques e Jardins', 'area_id' => '1'],
            ['id' => '14', 'nome' => 'Genética e Melhoramento dos Animais domésticos', 'area_id' => '1'],
            ['id' => '15', 'nome' => 'Manejo Florestal', 'area_id' => '1'],
            ['id' => '16', 'nome' => 'Máquinas e Implementos Agrícolas', 'area_id' => '1'],
            ['id' => '17', 'nome' => 'Maricultura', 'area_id' => '1'],
            ['id' => '18', 'nome' => 'Medicina Veterinária', 'area_id' => '1'],
            ['id' => '19', 'nome' => 'Nutrição e Alimentação Animal', 'area_id' => '1'],
            ['id' => '20', 'nome' => 'Patologia Animal', 'area_id' => '1'],
            ['id' => '21', 'nome' => 'Produção Agrícola', 'area_id' => '1'],
            ['id' => '22', 'nome' => 'Produção Animal', 'area_id' => '1'],
            ['id' => '23', 'nome' => 'Recursos Florestais', 'area_id' => '1'],
            ['id' => '24', 'nome' => 'Recursos Pesqueiros e Engenharia de Pesca', 'area_id' => '1'],
            ['id' => '25', 'nome' => 'Reprodução Animal', 'area_id' => '1'],
            ['id' => '26', 'nome' => 'Silvicultura', 'area_id' => '1'],
            ['id' => '27', 'nome' => 'Zootecnia', 'area_id' => '1'],


            // 2 - Ciencias Biologicas
            ['id' => '28', 'nome' => 'Biofísica', 'area_id' => '2'],
            ['id' => '29', 'nome' => 'Biologia Geral', 'area_id' => '2'],
            ['id' => '30', 'nome' => 'Bioquímica', 'area_id' => '2'],
            ['id' => '31', 'nome' => 'Biotecnologia', 'area_id' => '2'],
            ['id' => '32', 'nome' => 'Botânica', 'area_id' => '2'],
            ['id' => '33', 'nome' => 'Ciências ambientais', 'area_id' => '2'],
            ['id' => '34', 'nome' => 'Ecologia', 'area_id' => '2'],
            ['id' => '35', 'nome' => 'Farmacologia', 'area_id' => '2'],
            ['id' => '36', 'nome' => 'Fisiologia', 'area_id' => '2'],
            ['id' => '37', 'nome' => 'Genética', 'area_id' => '2'],
            ['id' => '38', 'nome' => 'Imunologia', 'area_id' => '2'],
            ['id' => '39', 'nome' => 'Microbiologia', 'area_id' => '2'],
            ['id' => '40', 'nome' => 'Morfologia', 'area_id' => '2'],
            ['id' => '41', 'nome' => 'Paleontologia', 'area_id' => '2'],
            ['id' => '42', 'nome' => 'Zoologia', 'area_id' => '2'],


            // 3 - Ciências da Saúde: 
            ['id' => '43', 'nome' => 'Análise e Controle de Medicamentos', 'area_id' => '3'],
            ['id' => '44', 'nome' => 'Análise Toxicológica', 'area_id' => '3'],
            ['id' => '45', 'nome' => 'Análises Clínicas', 'area_id' => '3'],
            ['id' => '46', 'nome' => 'Anatomia', 'area_id' => '3'],
            ['id' => '47', 'nome' => 'Patológica e Patologia Clínica', 'area_id' => '3'],
            ['id' => '48', 'nome' => 'Biomedicina', 'area_id' => '3'],
            ['id' => '49', 'nome' => 'Bioquímica da Nutrição', 'area_id' => '3'],
            ['id' => '50', 'nome' => 'Bromatologia', 'area_id' => '3'],
            ['id' => '51', 'nome' => 'Dietética', 'area_id' => '3'],
            ['id' => '52', 'nome' => 'Educação Física', 'area_id' => '3'],
            ['id' => '53', 'nome' => 'Enfermagem', 'area_id' => '3'],
            ['id' => '54', 'nome' => 'Epidemiologia', 'area_id' => '3'],
            ['id' => '55', 'nome' => 'Farmácia', 'area_id' => '3'],
            ['id' => '56', 'nome' => 'Farmacognosia', 'area_id' => '3'],
            ['id' => '57', 'nome' => 'Farmacotecnia', 'area_id' => '3'],
            ['id' => '58', 'nome' => 'Fisioterapia', 'area_id' => '3'],
            ['id' => '59', 'nome' => 'Fitoterapia', 'area_id' => '3'],
            ['id' => '60', 'nome' => 'Fonoaudiologia', 'area_id' => '3'],
            ['id' => '61', 'nome' => 'Homeopatia', 'area_id' => '3'],
            ['id' => '62', 'nome' => 'Medicina', 'area_id' => '3'],
            ['id' => '63', 'nome' => 'Medicina Preventiva', 'area_id' => '3'],
            ['id' => '64', 'nome' => 'Nutrição', 'area_id' => '3'],
            ['id' => '65', 'nome' => 'Odontologia', 'area_id' => '3'],
            ['id' => '66', 'nome' => 'Psiquiatria', 'area_id' => '3'],
            ['id' => '67', 'nome' => 'Radiologia', 'area_id' => '3'],
            ['id' => '68', 'nome' => 'Médica', 'area_id' => '3'],
            ['id' => '69', 'nome' => 'Saúde Coletiva', 'area_id' => '3'],
            ['id' => '70', 'nome' => 'Saúde Materno-infantil', 'area_id' => '3'],
            ['id' => '71', 'nome' => 'Saúde Pública', 'area_id' => '3'],
            ['id' => '72', 'nome' => 'Terapia Ocupacional', 'area_id' => '3'],


            //4 - Ciências Exatas e da Terra: 
            ['id' => '73', 'nome' => 'Astronomia', 'area_id' => '4'],
            ['id' => '74', 'nome' => 'Ciência da Computação', 'area_id' => '4'],
            ['id' => '75', 'nome' => 'Física', 'area_id' => '4'],
            ['id' => '76', 'nome' => 'Geociências', 'area_id' => '4'],
            ['id' => '77', 'nome' => 'Matemática', 'area_id' => '4'],
            ['id' => '78', 'nome' => 'Oceanografia', 'area_id' => '4'],
            ['id' => '79', 'nome' => 'Probabilidade e Estatística', 'area_id' => '4'],
            ['id' => '80', 'nome' => 'Química', 'area_id' => '4'],


            //5 - Ciências Humanas: 
            ['id' => '81', 'nome' => 'Antropologia', 'area_id' => '5'],
            ['id' => '82', 'nome' => 'Arqueologia', 'area_id' => '5'],
            ['id' => '83', 'nome' => 'Ciência Política', 'area_id' => '5'],
            ['id' => '84', 'nome' => 'Educação', 'area_id' => '5'],
            ['id' => '85', 'nome' => 'Filosofia', 'area_id' => '5'],
            ['id' => '86', 'nome' => 'Geografia', 'area_id' => '5'],
            ['id' => '87', 'nome' => 'História', 'area_id' => '5'],
            ['id' => '88', 'nome' => 'Psicologia', 'area_id' => '5'],
            ['id' => '89', 'nome' => 'Sociologia', 'area_id' => '5'],
            ['id' => '90', 'nome' => 'Teologia', 'area_id' => '5'],

            //6 - Ciências Sociais: 
            ['id' => '91', 'nome' => 'Administração', 'area_id' => '6'],
            ['id' => '92', 'nome' => 'Arquitetura e Urbanismo', 'area_id' => '6'],
            ['id' => '93', 'nome' => 'Arquivologia', 'area_id' => '6'],
            ['id' => '94', 'nome' => 'Artes', 'area_id' => '6'],
            ['id' => '95', 'nome' => 'Artes Plásticas', 'area_id' => '6'],
            ['id' => '96', 'nome' => 'Biblioteconomia', 'area_id' => '6'],
            ['id' => '97', 'nome' => 'Ciência da Informação', 'area_id' => '6'],
            ['id' => '98', 'nome' => 'Ciências Contábeis', 'area_id' => '6'],
            ['id' => '99', 'nome' => 'Cinema', 'area_id' => '6'],
            ['id' => '100', 'nome' => 'Comunicação', 'area_id' => '6'],
            ['id' => '101', 'nome' => 'Comunicação Visual', 'area_id' => '6'],
            ['id' => '102', 'nome' => 'Dança', 'area_id' => '6'],
            ['id' => '103', 'nome' => 'Decoração', 'area_id' => '6'],
            ['id' => '104', 'nome' => 'Demografia', 'area_id' => '6'],
            ['id' => '105', 'nome' => 'Desenho', 'area_id' => '6'],
            ['id' => '106', 'nome' => 'Desenho Industrial', 'area_id' => '6'],
            ['id' => '107', 'nome' => 'Designer', 'area_id' => '6'],
            ['id' => '108', 'nome' => 'Diplomacia', 'area_id' => '6'],
            ['id' => '109', 'nome' => 'Direito', 'area_id' => '6'],
            ['id' => '110', 'nome' => 'Economia', 'area_id' => '6'],
            ['id' => '111', 'nome' => 'Economia Doméstica', 'area_id' => '6'],
            ['id' => '112', 'nome' => 'Educação Artística', 'area_id' => '6'],
            ['id' => '113', 'nome' => 'Estudos Sociais', 'area_id' => '6'],
            ['id' => '114', 'nome' => 'Fotografia', 'area_id' => '6'],
            ['id' => '115', 'nome' => 'Jornalismo e Editoração', 'area_id' => '6'],
            ['id' => '116', 'nome' => 'Letras', 'area_id' => '6'],
            ['id' => '117', 'nome' => 'Língua Portuguesa', 'area_id' => '6'],
            ['id' => '118', 'nome' => 'Línguas Clássicas', 'area_id' => '6'],
            ['id' => '119', 'nome' => 'Línguas Estrangeiras Modernas', 'area_id' => '6'],
            ['id' => '120', 'nome' => 'Línguas Indígenas', 'area_id' => '6'],
            ['id' => '121', 'nome' => 'Linguística', 'area_id' => '6'],
            ['id' => '122', 'nome' => 'Literatura', 'area_id' => '6'],
            ['id' => '123', 'nome' => 'Moda', 'area_id' => '6'],
            ['id' => '124', 'nome' => 'Museologia', 'area_id' => '6'],
            ['id' => '125', 'nome' => 'Música', 'area_id' => '6'],
            ['id' => '126', 'nome' => 'Paisagismo', 'area_id' => '6'],
            ['id' => '127', 'nome' => 'Planejamento Urbano e Regional', 'area_id' => '6'],
            ['id' => '128', 'nome' => 'Política Pública e População', 'area_id' => '6'],
            ['id' => '129', 'nome' => 'Rádio e Televisão', 'area_id' => '6'],
            ['id' => '130', 'nome' => 'Relações Públicas e Propaganda', 'area_id' => '6'],
            ['id' => '131', 'nome' => 'Serviço Social', 'area_id' => '6'],
            ['id' => '132', 'nome' => 'Teatro', 'area_id' => '6'],
            ['id' => '133', 'nome' => 'Turismo', 'area_id' => '6'],

            // 7 - Engenharias e suas Aplicações: 
            ['id' => '134', 'nome' => 'Engenharia Aeroespacial', 'area_id' => '7'],
            ['id' => '135', 'nome' => 'Engenharia Biomédica', 'area_id' => '7'],
            ['id' => '136', 'nome' => 'Engenharia Cartográfica', 'area_id' => '7'],
            ['id' => '137', 'nome' => 'Engenharia Civil', 'area_id' => '7'],
            ['id' => '138', 'nome' => 'Engenharia de Agrimensura', 'area_id' => '7'],
            ['id' => '139', 'nome' => 'Engenharia de Armamentos', 'area_id' => '7'],
            ['id' => '140', 'nome' => 'Engenharia de Materiais e Metalúrgica', 'area_id' => '7'],
            ['id' => '141', 'nome' => 'Engenharia de Minas', 'area_id' => '7'],
            ['id' => '142', 'nome' => 'Engenharia de Produção', 'area_id' => '7'],
            ['id' => '143', 'nome' => 'Engenharia de Transportes', 'area_id' => '7'],
            ['id' => '144', 'nome' => 'Engenharia Elétrica', 'area_id' => '7'],
            ['id' => '145', 'nome' => 'Engenharia Eletrônica', 'area_id' => '7'],
            ['id' => '146', 'nome' => 'Engenharia Mecânica', 'area_id' => '7'],
            ['id' => '147', 'nome' => 'Engenharia Mecatrônica', 'area_id' => '7'],
            ['id' => '148', 'nome' => 'Engenharia Naval e Oceânica', 'area_id' => '7'],
            ['id' => '149', 'nome' => 'Engenharia Nuclear', 'area_id' => '7'],
            ['id' => '150', 'nome' => 'Engenharia Química', 'area_id' => '7'],
            ['id' => '151', 'nome' => 'Engenharia Sanitária', 'area_id' => '7'],
            ['id' => '152', 'nome' => 'Engenharia Têxtil', 'area_id' => '7'],
            ['id' => '153', 'nome' => 'Engenharia Agrícola', 'area_id' => '7'],
            ['id' => '154', 'nome' => 'Engenharia Florestal', 'area_id' => '7'],
            ['id' => '155', 'nome' => 'Engenharia ambiental', 'area_id' => '7'],
            ['id' => '156', 'nome' => 'Engenharia de alimentos', 'area_id' => '7'],
            ['id' => '157', 'nome' => 'Engenharia da computação', 'area_id' => '7'],
            ['id' => '158', 'nome' => 'Robótica', 'area_id' => '7']
       ]);
    }
}
