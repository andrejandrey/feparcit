<?php

use Illuminate\Database\Seeder;

class perfil extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            ['id' => '1','name' => 'admin'],
            ['id' => '2','name' => 'avaliador'],
            ['id' => '3','name' => 'voluntario'],
            ['id' => '4','name' => 'aluno'],
        ]);
    }
}
