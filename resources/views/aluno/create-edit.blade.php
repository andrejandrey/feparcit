@extends('layouts.app')
@section('content')
<div class="page-content edit-add container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-bordered">
                <h1 class="page-title"><i class="voyager-alarm-clock"></i>{{isset($aluno) ? 'Editar dados do aluno' :
                    'Cadastrar aluno'}}</h1>
                @if ($errors->any())
                <div class='alert alert-danger'>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (isset($aluno))
                <form method="post" action="{{route('alunos.update', ['id'=>$aluno->id])}}" enctype="multipart/form-data">
                    {!! method_field('PUT')!!}
                    @else
                    <form action="{{route('alunos.store')}}" method='post' enctype="multipart/form-data">
                        @endif
                        {{ csrf_field() }}
                        <div class="row">
                            <div class='form-group col-md-8'>
                                <label>Nome</label>
                                <input type="text" name='nome' value='{{$aluno->nome ?? old("nome")}}' class='form-control' />
                            </div>

                            <div class='form-group col-md-4'>
                                <label>RG</label>
                                <input type="text" name='rg' value='{{$aluno->nome ?? old("nome")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>CPF</label>
                                <input type="text" name='cpf' value='{{$aluno->cpf ?? old("cpf")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                    <label>Sexo</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexo" value="M"
                                            @if(isset($voluntario) && ($voluntario->sexo == "M")) 
                                                checked 
                                            @else
                                                {{ old('sexo')=="M" ? 'checked='.'"'.'checked'.'"' : '' }}
                                            @endif >
                                        <label class="form-check-label" for="inlineRadio1">Masculino</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexo" value="F"
                                            @if(isset($voluntario) && ($voluntario->sexo == "F")) 
                                                checked 
                                            @else
                                                {{ old('sexo')=="F" ? 'checked='.'"'.'checked'.'"' : '' }}
                                            @endif >
                                        <label class="form-check-label" for="inlineRadio1">Feminino</label>
                                    </div>
                                </div>



                            <div class='form-group col-md-4'>
                                <label>Telefone</label>
                                <input type="text" name='telefone' value='{{$aluno->telefone ?? old("telefone")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>Celular</label>
                                <input type="text" name='celular' value='{{$aluno->celular ?? old("celular")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>Data de Nascimento</label>
                                <input type="date" name='data_nascimento' value='{{$aluno->data_nascimento ?? old("data_nascimento")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>cep</label>
                                <input type="text" name='cep' value='{{$aluno->cep ?? old("cep")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-8'>
                                <label>Endereço</label>
                                <input type="text" name='endereco' value='{{$aluno->endereco ?? old("endereco")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>Cidade</label>
                                <input type="text" name='cidade' value='{{$aluno->cidade ?? old("cidade")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-7'>
                                <label>Email</label>
                                <input type="email" name='email' value='{{$aluno->email ?? old("email")}}' class='form-control' />
                            </div>

                            @if (!isset($participante))
                            <div class='form-group col-md-5'>
                                <label>Senha</label>
                                <input type="password" name='senha' value='{{$aluno->senha ?? old("senha")}}' class='form-control' />
                            </div>
                            @endif
                            <div class="form-group col-md-4">
                                <label>Tamanho camiseta</label>
                                <select name="tamanho_camiseta" class="form-control">
                                    <option value="">Selecione</option>
                                    <option value="P"  
                                        @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "P")) 
                                            selected 
                                        @else
                                            {{ old('tamanho_camiseta')=="P" ? 'selected' : '' }}
                                        @endif>P</option>
                                    <option value="M"
                                        @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "M")) 
                                            selected 
                                        @else
                                            {{ old('tamanho_camiseta')=="M" ? 'selected' : '' }}
                                        @endif>M</option>
                                    <option value="G"
                                        @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "G")) 
                                            selected 
                                        @else
                                            {{ old('tamanho_camiseta')=="G" ? 'selected' : '' }}
                                        @endif>G</option>
                                    <option value="GG"
                                        @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "GG")) 
                                            selected 
                                        @else
                                            {{ old('tamanho_camiseta')=="GG" ? 'selected' : '' }}
                                        @endif>GG</option>
                                </select>
                            </div>


                            <div class="form-group col-md-4">
                                <label>Necessidade especial</label>
                                <select name="possui_necessidade_especial" class="form-control">
                                    <option value="0">Sem necessidade especial</option>
                                    <option value="1">Com necessidade especial</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Caso possua necessidade especial informe</label>
                                <input type="text" name="necessidade_especial" value='{{$aluno->necessidade_especial ?? old("necessidade_especial")}}' class="form-control" />
                                  
                            </div>
                            
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <div style="padding-left: 15px; padding-bottom: 15px;">
                                <!--   <button type="submit" class="btn btn-primary save">Salvar</button> -->
                                @if (isset($aluno))
                                <button type='submit' class='btn btn-primary '>Alterar</button>
                                @else
                                <button type="submit" class="btn btn-primary ">Salvar</button>
                                @endif
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@stop