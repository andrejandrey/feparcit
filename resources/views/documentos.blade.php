<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>FEPARCIT</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!--
			CSS
			============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/estilo.css">
    <style>
        body{
            background: none;
        }
    </style>
</head>

<body>
    <!-- Start info Area -->
    <section class="info-area" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-30">

                </div>
            </div>
            <div class="row">
                <h1 class="m-5">Biblioteca de documentos</h1>
                <table class="table table-striped">
                    <tr>
                        <th width="90%">Descrição do documento</th>
                        <th width="10%" colspan="2">Tipo de download</th>
                    </tr>
                    <tr>
                        <td>Autorização de direitos intelectuais e uso de imagem para maiores de idade</td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_direitos_intelectuais_e_uso_de_imagem_para_maiores_de_idade.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_direitos_intelectuais_e_uso_de_imagem_para_maiores_de_idade.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Autorização de direitos intelectuais e uso de imagem para menores de idade</td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_direitos_intelectuais_e_uso_de_imagem_para_menores_de_idade.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_direitos_intelectuais_e_uso_de_imagem_para_menores_de_idade.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>

                        <td>Autorização de hospedagem para menores de idade</td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_hospedagem_para_menores_de_idade.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_hospedagem_para_menores_de_idade.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Autorização de viagem para alunos menores de idade</td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_viagem_para_alunos_menores_de_idade.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/autorizacao_de_viagem_para_alunos_menores_de_idade.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Formulários para pesquisa com agentes biológicos potencialmente perigosos</td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_agentes_biologicos_potencialmente_perigosos.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_agentes_biologicos_potencialmente_perigosos.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Formulários para pesquisa com animais vertebrados</td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_animais_vertebrados.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_animais_vertebrados.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Formulários para pesquisa com equipamentos de alta periculosidade</td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_equipamentos_de_alta_periculosidade.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_equipamentos_de_alta_periculosidade.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Formulários para pesquisa com seres humanos</td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_seres_humanos.docx') }}">
                                <img src="{{ asset('img/docx.png') }}" width="50" />
                            </a>
                        </td>
                        <td>
                            <a href="{{ asset('docs/formularios_para_pesquisa_com_seres_humanos.pdf') }}">
                                <img src="{{ asset('img/pdf.png') }}" width="50" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                            <td>Formulários para pesquisa com substâncias tóxicas ou controladas</td>
                            <td>
                                <a href="{{ asset('docs/formularios_para_pesquisa_com_substancias_toxicas_ou_controladas.docx') }}">
                                    <img src="{{ asset('img/docx.png') }}" width="50" />
                                </a>
                            </td>
                            <td>
                                <a href="{{ asset('docs/formularios_para_pesquisa_com_substancias_toxicas_ou_controladas.pdf') }}">
                                    <img src="{{ asset('img/pdf.png') }}" width="50" />
                                </a>
                            </td>
                        </tr>
                        <tr>
                                <td>Formulários para pesquisa com tecidos humanos ou de outros vertebrados</td>
                                <td>
                                    <a href="{{ asset('docs/formularios_para_pesquisa_com_tecidos_humanos_ou_de_outros_vertebrados.docx') }}">
                                        <img src="{{ asset('img/docx.png') }}" width="50" />
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ asset('docs/formularios_para_pesquisa_com_tecidos_humanos_ou_de_outros_vertebrados.pdf') }}">
                                        <img src="{{ asset('img/pdf.png') }}" width="50" />
                                    </a>
                                </td>
                            </tr>
                </table>


                <br>
            </div>
        </div>
    </section>
    <!-- End info Area -->

    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="js/easing.min.js"></script>
    <script src="js/hoverIntent.js"></script>
    <script src="js/superfish.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
</body>

</html>