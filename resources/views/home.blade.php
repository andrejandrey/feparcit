@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @if (session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
                @endif
                <div class="card-header">Meu projeto</div>
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <div class="card-body">
                    
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                1° Passo - Cadastrar seu projeto
                              </button>
                            </h5>
                          </div>
                      
                          <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                              @if (isset($projeto))
                                <table class="table">
                                  <tr>
                                    <th width="90%">Projeto</th><th>Ação</th>
                                  </tr>
                                  <tr>
                                    <td>{{$projeto->nome}}</td>
                                    <td><a href="{{ route('projects.edit', $projeto->id) }}" title="Editar" class="btn btn-sm btn-primary pull-right edit">
                                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Editar</span>
                                    </a></td>
                                  </tr>
                                </table>
                              @else
                                <a href="{{ route('projects.create') }}" class="btn btn-success"> Cadastrar projeto </a>
                              @endif
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2° Passo - Adicionar dados dos participantes 
                              </button>
                            </h5>
                          </div>
                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                              @if (isset($projeto))
                                <table class="table">
                                  <tr>
                                    <th width="90%">Participante</th><th>Ação</th>
                                  </tr>
                                  @foreach ($alunos as $a)
                                  <tr>
                                    <td>{{$a->nome}}</td>
                                    <td>
                                      <a href="{{ route('alunos.edit', $a->id) }}" title="Editar" class="btn btn-sm btn-primary pull-right edit">
                                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Editar</span>
                                      </a>
                                    </td>  
                                  </tr>  
                                  @endforeach
                                  <tr>
                                    <td></td>
                                    <td>
                                      @if (isset($alunos) && count($alunos) <= 3)
                                      <a href="{{ route('participante.create') }}" title="Incluir aluno" class="btn btn-sm btn-success pull-right edit">
                                          <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Adicionar participante</span>
                                      </a>
                                      @endif
                                    </td>
                                  </tr>
                                </table>
                              @else
                              {{--   <a href="{{ route('projects.create') }}"  class="btn btn-success"> Enviar projeto </a> --}}
                              <p>Realize o primeiro passo para habilitar este item</p>
                              @endif
                              
                            </div>
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                              <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                3° Passo - Adicionar dados do orientador
                              </button>
                            </h5>
                          </div>
                          <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                                @if (isset($projeto))
                                <table class="table">
                                  <tr>
                                    <th width="90%">Orientador</th><th>Ação</th>
                                  </tr>
                                  @if (isset($orientador) && count($orientador) > 0)
                                  @foreach ($orientador as $o)
                                  <tr>
                                    <td>{{$o->nome}}</td>
                                    <td>
                                      <a href="{{ route('orientadors.edit', $o->id) }}" title="Editar" class="btn btn-sm btn-primary pull-right edit">
                                        <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Editar</span>
                                      </a>
                                    </td>  
                                  </tr>  
                                  @endforeach
                                  @else 
                                    <tr>
                                      <td></td>
                                      <td>
                                        <a href="{{ route('orientadors.create') }}" title="Incluir orientador" class="btn btn-sm btn-success pull-right edit">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Adicionar Orientador</span>
                                        </a>
                                      </td>
                                    </tr>
                                  @endif

                                </table>

                                <table class="table">
                                    <tr>
                                      <th width="90%">Coorientador</th><th>Ação</th>
                                    </tr>
                  
                                    @foreach ($coorientador as $co)
                                    <tr>
                                      <td>{{$co->nome}}</td>
                                      <td>
                                        <a href="{{ route('orientadors.edit', $co->id) }}" title="Editar" class="btn btn-sm btn-primary pull-right edit">
                                          <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Editar</span>
                                        </a>
                                       {{--  <a href="{{ route('orientadors.edit', $co->id) }}" title="Editar" class="btn btn-sm btn-primary pull-right edit">
                                          <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Editar</span>
                                        </a> --}}
                                      </td>  
                                    </tr>  
                                    @endforeach
                                   
                                      <tr>
                                        <td></td>
                                        <td>
                                          <a href="{{ route('orientadors.create') }}" title="Incluir orientador" class="btn btn-sm btn-success pull-right edit">
                                              <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Adicionar Coorientador</span>
                                          </a>
                                        </td>
                                      </tr>
                                   
  
                                  </table>
                              @else
                                <p>Realize o primeiro passo para habilitar este item</p>

                               {{--  <a href="{{ route('projects.create') }}"  class="btn btn-success"> Enviar projeto </a> --}}
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
