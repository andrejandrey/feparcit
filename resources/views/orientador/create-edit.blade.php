@extends('layouts.app')
@section('content')
<div class="page-content edit-add container-fluid">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-bordered">
                <h1 class="page-title"><i class="voyager-alarm-clock"></i>{{isset($orientador) ? 'Editar dados do orientador/coorientador' :  'Cadastrar orientador/coorientador'}}</h1>
                @if ($errors->any())
                <div class='alert alert-danger'>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (isset($orientador))
                <form method="post" action="{{route('orientadors.update', ['id'=>$orientador->id])}}" enctype="multipart/form-data">
                    {!! method_field('PUT')!!}
                    @else
                    <form action="{{route('orientadors.store')}}" method='post' enctype="multipart/form-data">
                        @endif
                        {{ csrf_field() }}
                      
                        <div class="row">
                            @if (isset($tipo_orientador))
                                <div class="form-group col-md-4">
                                    <label>Tipo</label>
                                    <select name="tipo_orientador" class="form-control">
                                        @if (isset($tipo_orientador) && $tipo_orientador!=1))
                                            <option value="1">Orientador</option>
                                        @endif
                                        <option value="2">Coorientador</option>
                                    </select>
                                </div>
                            @endif
                            <div class='form-group col-md-8'>
                                <label>Nome</label>
                                <input type="text" name='nome' value='{{$orientador->nome ?? old("nome")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                    <label>CPF</label>
                                    <input type="text" name='cpf' value='{{$orientador->cpf ?? old("cpf")}}' class='form-control' />
                                </div>
                            <div class='form-group col-md-4'>
                                <label>RG</label>
                                <input type="text" name='rg' value='{{$orientador->rg ?? old("rg")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                    <label>Sexo</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexo" value="M"
                                            @if(isset($voluntario) && ($voluntario->sexo == "M")) 
                                                checked 
                                            @else
                                                {{ old('sexo')=="M" ? 'checked='.'"'.'checked'.'"' : '' }}
                                            @endif >
                                        <label class="form-check-label" for="inlineRadio1">Masculino</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="sexo" value="F"
                                            @if(isset($voluntario) && ($voluntario->sexo == "F")) 
                                                checked 
                                            @else
                                                {{ old('sexo')=="F" ? 'checked='.'"'.'checked'.'"' : '' }}
                                            @endif >
                                        <label class="form-check-label" for="inlineRadio1">Feminino</label>
                                    </div>
                                </div>

                                 
                   
                            <div class='form-group col-md-4'>
                                    <label>Telefone</label>
                                    <input type="text" name='telefone' value='{{$orientador->telefone ?? old("telefone")}}' class='form-control' />
                                </div>
                                <div class='form-group col-md-4'>
                                        <label>Celular</label>
                                        <input type="text" name='celular' value='{{$orientador->celular ?? old("celular")}}' class='form-control' />
                                    </div>
                            <div class='form-group col-md-4'>
                                <label>Data de Nascimento</label>
                                <input type="date" name='data_nascimento' value='{{$orientador->data_nascimento ?? old("data_nascimento")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>cep</label>
                                <input type="text" name='cep' value='{{$orientador->cep ?? old("cep")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>Endereço</label>
                                <input type="text" name='endereco' value='{{$orientador->endereco ?? old("endereco")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                <label>Cidade</label>
                                <input type="text" name='cidade' value='{{$orientador->cidade ?? old("cidade")}}' class='form-control' />
                            </div>
                            <div class='form-group col-md-4'>
                                    <label>Email</label>
                                    <input type="email" name='email' value='{{$orientador->email ?? old("email")}}' class='form-control' />
                                </div>
                           


                            <div class="form-group col-md-4">
                                    <label>Necessidade especial</label>
                                    <select name="possui_necessidade_especial" class="form-control">
                                        <option value="0">Sem necessidade especial</option>
                                        <option value="1">Com necessidade especial</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Caso possua necessidade especial informe</label>
                                    <input type="text" name="necessidade_especial" value='{{$avaliador->necessidade_especial ?? old("necessidade_especial")}}' class="form-control" />
                                      
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Tamanho camiseta</label>
                                    <select name="tamanho_camiseta" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="P"  
                                            @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "P")) 
                                                selected 
                                            @else
                                                {{ old('tamanho_camiseta')=="P" ? 'selected' : '' }}
                                            @endif>P</option>
                                        <option value="M"
                                            @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "M")) 
                                                selected 
                                            @else
                                                {{ old('tamanho_camiseta')=="M" ? 'selected' : '' }}
                                            @endif>M</option>
                                        <option value="G"
                                            @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "G")) 
                                                selected 
                                            @else
                                                {{ old('tamanho_camiseta')=="G" ? 'selected' : '' }}
                                            @endif>G</option>
                                        <option value="GG"
                                            @if(isset($voluntario) && ($voluntario->tamanho_camiseta == "GG")) 
                                                selected 
                                            @else
                                                {{ old('tamanho_camiseta')=="GG" ? 'selected' : '' }}
                                            @endif>GG</option>
                                    </select>
                                </div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <div style="padding-left: 15px; padding-bottom: 15px;">
                                <!--   <button type="submit" class="btn btn-primary save">Salvar</button> -->
                                @if (isset($orientador))
                                <button type='submit' class='btn btn-primary '>Alterar</button>
                                @else
                                <button type="submit" class="btn btn-primary ">Salvar</button>
                                @endif
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@stop