@extends('layouts.app')
@section('content')
<div class="page-content edit-add container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-bordered">
                <h1 class="page-title"><i class="voyager-alarm-clock"></i>{{isset($project) ? 'Editar projeto' :
                    'Cadastrar projeto'}}</h1>
                @if ($errors->any())
                <div class='alert alert-danger'>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (isset($project))
                <form method="post" action="{{route('projects.update', ['id'=>$project->id])}}" enctype="multipart/form-data">
                    {!! method_field('PUT')!!}
                    @else
                    <form action="{{route('projects.store')}}" method='post' enctype="multipart/form-data">
                        @endif
                        {{ csrf_field() }}

                        <div class="panel-body row">
                            <div class="form-group col-md-12">
                                <label>Categoria</label>
                                <select name="categoria" class="form-control">
                                    <option value="">selecione</option>
                                    <option value="1" 
                                        @if(isset($project) && ($project->categoria == 1))
                                            selected
                                        @else
                                            {{ old('categoria') == 1 ? 'selected' : '' }}
                                        @endif
                                        
                                    >Ensino médio</option>
                                    <option value="2"
                                        @if(isset($project) && ($project->categoria == 2))
                                            selected
                                        @else
                                            {{ old('categoria') == 2 ? 'selected' : '' }}
                                        @endif
                                    >Ensino pós médio (Subsequente)</option>
                                </select>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Área do conhecimento</label>
                                <select name="area_id" id="areas" class="form-control">
                                    <option value="">selecione</option>
                                    @foreach ($areas as $a)

                                    <option value="{{$a->id}}" @if(isset($project) && ($project->area_id ==
                                        $a->id))
                                        selected
                                        @else
                                            {{ old('area_id') == $a->id ? 'selected' : '' }}
                                        @endif
                                        >{{$a->nome}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                    <label>Sub Área do conhecimento</label>
                                    <select name="subarea_id" id="subareas" class="form-control">
                                        <option value="">selecione</option>
                                    </select>
                                </div>
                            <div class='form-group col-md-12'>
                                <label>Título do projeto</label>
                                <input type="text" name='nome' value='{{$project->nome ?? old("nome")}}' class='form-control' />
                            </div>

                            <div class='form-group col-md-12'>
                                <label>Problema</label>
                                <textarea name='problema' class='form-control'>{{$project->problema ?? old("problema")}}</textarea>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Hipótese ou objetivo de engenharia</label>
                                <textarea name='hipotese' class='form-control'>{{$project->hipotese ?? old("nome")}}</textarea>
                            </div>

                            <div class='form-group col-md-12'>
                                <label>Métodos</label>
                                <textarea name='metodos' class='form-control'>{{$project->metodos ?? old("metodos")}}</textarea>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Materiais</label>
                                <textarea name='materiais' class='form-control'>{{$project->materiais ?? old("materiais")}}</textarea>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Cronograma</label>
                                <input type="file" name='cronograma' value='{{$project->cronograma ?? old("cronograma")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Resultados</label>
                                <textarea name='resultado' class='form-control'>{{$project->resultado ?? old("resultado")}}</textarea>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Bibliografia</label>
                                <textarea name='bibliografia' class='form-control'>{{$project->bibliografia ?? old("bibliografia")}}</textarea>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Resumo (Máximo 2000 caracteres)</label>
                                <textarea name='resumo' class='form-control'>{{$project->resumo ?? old("resumo")}}</textarea>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Palavras – chave</label>
                                <input type="text" name='palavras_chave' value='{{$project->palavras_chave ?? old("palavras_chave")}}'
                                    class='form-control' />
                            </div>
                            <div class="form-group col-md-12">
                                <label>Pesquisa</label>
                                <select name="tipo_pesquisa" id="tipo_pesquisa" class="form-control">
                                    @foreach ($tipo_pesquisa as $t)

                                    <option value="{{$t['id']}}" 
                                    @if(isset($project) && ($project->tipo_pesquisa == $t['id']))
                                        selected
                                    @else
                                        {{ old('tipo_pesquisa') == $t['id'] ? 'selected' : '' }}
                                    @endif
                                        >{{$t['name']}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="d-none col-md-12" id="anexar_parecer">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Parecer comite de Ética</label>
                                        <input type="file" name='parecer' value='{{$project->parecer ?? old("parecer")}}'
                                            class='form-control' />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>&nbsp;</label>
                                        <a href="{{route('documentos')}}" target="blank" class="btn btn-primary form-control">Download modelo</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label>Tipo de instituição de ensino</label>
                                <select name="tipo_instituicao" class="form-control">
                                    @foreach ($tipo_instituicao as $t)

                                    <option value="{{$t['id']}}" @if(isset($project) && ($project->tipo_instituicao ==
                                        $t['id']))
                                        selected
                                        @endif
                                        >{{$t['name']}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Nome Instituição</label>
                                <input type="text" name='nome_instituicao' value='{{$project->nome_instituicao ?? old("nome_instituicao")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Email Instituição</label>
                                <input type="text" name='email_instituicao' value='{{$project->email_instituicao ?? old("email_instituicao")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Telefone Instituição</label>
                                <input type="text" name='telefone_instituicao' value='{{$project->telefone_instituicao ?? old("telefone_instituicao")}}'
                                    class='form-control' />
                            </div>
                            <div class='form-group col-md-12'>
                                <label>Anexo</label>
                                <input type="file" name='anexo' value='{{$project->anexo ?? old("anexo")}}' class='form-control' />
                            </div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <div style="padding-left: 15px; padding-bottom: 15px;">
                                <!--   <button type="submit" class="btn btn-primary save">Salvar</button> -->
                                @if (isset($project))
                                <button type='submit' class='btn btn-primary '>Alterar</button>
                                @else
                                <button type="submit" class="btn btn-primary ">Salvar</button>
                                @endif
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<script>
    $( document ).ready(function() {
        



        $("#tipo_pesquisa").on('change', function (e) {
        
            var tipo_pesquisa = e.target.value;

            if (tipo_pesquisa < 5) {
                $("#anexar_parecer").removeClass('d-none');
            } else {
                $("#anexar_parecer").addClass('d-none');
            }
        });

        $("#areas").on('change', function(e){
        var area = e.target.value;
        console.log(area);
        $.get("/ajax-subareas?area=" + area, function(data){
            console.log(data);
            $("#subareas").empty();             
            $("#subareas").append("<option value=''>Selecione</option>");
            $.each(data, function(index, subarea){
                //console.log(subarea);
            if(subarea.id == @php echo (null !== old('subarea_id')) ?  old('subarea_id') : '-1'@endphp )
                $("#subareas").append("<option value='"+ subarea.id +"' selected>" + subarea.nome + "</option>");
            else
                $("#subareas").append("<option value='"+ subarea.id +"'>" + subarea.nome + "</option>");
            });
        });
    });
    $("#areas").trigger('change');
    });
</script>
@stop