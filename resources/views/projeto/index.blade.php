@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="voyager-categories"></i> Projetos
        <a href="{{ route('home') }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>Voltar</span>
        </a>
    </h1>

</div>

<div id="voyager-notifications">
    @foreach (['danger', 'warning', 'success', 'info'] as $key)
    @if(Session::has($key))
    <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
    @endif
    @endforeach
</div>


<div class="page-content browse container-fluid">
    <div class="alerts">
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table id="dataTable3" class="table table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Nome</th>
                                    <th>Categoria</th>
                                    <th>Área</th>
                                    <th>Sub Área</th>
                                    <th>Alunos</th>
                                    <th>Orientadores</th>
                                    {{-- <th class="actions text-right">Ações</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($projetos as $p)
                                <tr>
                                    <td>
                                        @php
                                        $date = new DateTime($p->created_at);
                                        echo $date->format('d/m/Y');

                                        @endphp
                                    </td>
                                    <td>{{$p->nome}}</td>

                                    <td>{{$p->categoria=='2' ? 'Subsequente' : 'Ensino médio'}}</td>

                                    <td>{{$p->subarea->area->nome}}</td>
                                    <td>{{$p->subarea->nome}}</td>

                                    <td>{{$p->user->name}}<br />
                                        @foreach ($p->alunos as $aluno)
                                        {{$aluno->nome}} <br />
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach ($p->orientadores as $orientador)
                                        {{$orientador->nome}} <br />
                                        @endforeach
                                    </td>
                                    {{-- <td>{{$p->orientadores}}</td> --}}
                                    {{-- <td>
                                        <a href="javascript:;" title="Remover" class="btn btn-sm btn-danger pull-right delete"
                                            data-id="{{$c->id}}" id="delete-2">
                                            <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">Remover</span>
                                        </a>
                                        <a href="{{ route('categories.edit', $c->id) }}" title="Editar" class="btn btn-sm btn-primary pull-right edit">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Editar</span>
                                        </a>
                                        <a href="{{ route('clients.show', $c->id) }}" title="Ver" class="btn btn-sm btn-warning pull-right view">
                                            <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">Ver</span>
                                        </a>
                                    </td>--}}
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="voyager-trash"></i> Tem certeza de que deseja remover esta categoria?</h4>
            </div>
            <div class="modal-footer">
                <form action="#" id="delete_form" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" class="btn btn-danger pull-right delete-confirm" value="Sim, Remover!">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
</div>
</div>
</div>

<!-- DataTables -->
<script>
    $(document).ready(function () {
        var table = $('#dataTable3').DataTable({
            responsive: true,
            "order": [],
            "language": {
                "sEmptyTable": "N\u00e3o h\u00e1 registros para apresentar",
                "sInfo": "Mostrando de _START_ at\u00e9 _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando de 0 at\u00e9 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
                "sInfoPostFix": "",
                "sInfoThousands": ",",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sLoadingRecords": "A Carregar...",
                "sProcessing": "A processar...",
                "sSearch": "Procurar:",
                "sZeroRecords": "N\u00e3o foram encontrados resultados",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sLast": "\u00daltimo",
                    "sNext": "Seguinte",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": ativar para ordenar de forma crescente",
                    "sSortDescending": ": ativar para ordenar de forma decrescente"
                }
            },
            columnDefs: [
                { responsivePriority: 1, targets: 0 },
                { responsivePriority: 2, targets: -1 }
            ]
        });

        $('.select_all').on('click', function (e) {
            $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
        });
    });


    var deleteFormAction;
    $('td').on('click', '.delete', function (e) {
        $('#delete_form')[0].action = 'categories/__id'.replace('__id', $(this).data('id'));
        $('#delete_modal').modal('show');
    });

</script>
@stop