@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        @if (session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
        @endif
        <div class="card-header">Página do Voluntário</div>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
          {{ session('status') }}
        </div>
        @endif
        <div class="card-body">
          <div class="alert alert-success">
            <br />
            <h3>Parabéns seu cadastro foi registrado.</h3>
            <br />
            <p>Aguarde que nós entraremos em contado com você no email cadastrado para passar mais informações!</p>
            <br />
            <br />

            <p><b>Obrigado!</b></p>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
@endsection