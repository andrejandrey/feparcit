<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth', 'periodo.inscricao']], function() {
    Route::resource('projects', 'ProjetoController');
    Route::resource('orientadors', 'OrientadorController');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('ajax-subareas', 'ProjetoController@getSubAreas')->name('getSubAreas');
});

Route::resource('alunos', 'AlunoController');
Route::resource('avaliadores', 'AvaliadorController');
Route::resource('voluntarios', 'VoluntarioController');
Route::post('/contato', 'ContactController@post')->name('contato');

Route::get('participante', 'AlunoController@participante')->name('participante.create');
Route::get('documentos', 'HomeController@documentos')->name('documentos');


Auth::routes();

