#!/bin/bash

echo Copying the configuration example file
cp .env.example .env

composer require predis/predis

echo Generate key
php artisan key:generate

#echo Generate Session
#php artisan session:table

echo Generate key
php artisan storage:link

echo Make migrations
php artisan migrate

#echo Generate seed
#php artisan db:seed

#echo Generate JWT
#php artisan jwt:secret
